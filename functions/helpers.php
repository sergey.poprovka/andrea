<?php
    function parse_date($date, $format = "%b %d, %Y"){
        if(!preg_match("/[0-9]*/",$date)){
            $date = strtotime($date);
        }
        return strftime($format, $date);
    }

    function parse_content($content, $start = 0, $limit = 150){
        $string = mb_substr($content, $start, $limit);
        if(strlen($content) > 150){
            $string .= "...";
        }
        return $string;
    }
?>