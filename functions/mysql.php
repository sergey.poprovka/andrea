<?php
    require 'config.php';

    function db_connect(){
        $mysql = mysqli_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB) or die('Can\'t connect mysql');
        return $mysql;
    }

    function extract_columns($table, $aliases = true){
        $list = [];
        $columns = db_raw_query("DESCRIBE " . $table);
        foreach($columns as $column){
            if($aliases) {
                $list[] = $table.".".$column['Field']." AS `".$table."-".$column['Field']."`";
            }else{
                $list[] = $table.".".$column['Field'];
            }
        }

        return implode(', ',$list);
    }

    function add_left_joins($table, $joins){
        $string = "";
        foreach($joins as $join){
            $string .= " LEFT JOIN `". $join[0] ."` ON `".$table."`.`".$join[1]."`=`".$join[0]."`.`".$join[2]."`";
        }

        return $string;
    }

    function db_query($table_name, $params = [], $joins = [], $limit = PER_PAGE, $start = 0){
        $query = "SELECT ";
        $columns = "*";
        if(count($joins)){
            $columns = extract_columns($table_name, false) . ", ";
            $i = 0;
            foreach($joins as $join){
                $i += 1;
                $columns .= extract_columns($join[0]);
                if(count($joins) != $i){
                    $columns .= ', ';
                }
            }
        }
        $query .= $columns . " FROM `".$table_name."`";
        if(count($joins)){
            $query .= add_left_joins($table_name, $joins);
        }
        if(count($params)){
            $query .= " WHERE ";
            $i = 0;
            foreach($params as $param){
                $i += 1;
                if($i > 1) $query .= " AND ";
                switch(count($param)){
                    case 2:
                        $query .= "`".$table_name."`.`".$param[0]."`='".$param[1]."'";
                        break;
                    case 3:
                        $query .= "`".$table_name."`.`".$param[0]."` ".$param[1]." '".$param[2]."'";
                        break;
                }
            }
        }
        $query .= " LIMIT " . $start .", ".$limit;
//        echo $query;
//        die();
        return db_raw_query($query, count($joins) ? true : false);
    }

    function db_raw_query($query, $formatted = false){
        $res = mysqli_query(db_connect(), $query);

        $query_results = [];

        if(is_bool($res)){
            return $query_results;
        }

        while ($line = mysqli_fetch_assoc($res)) {
            $query_results[] = $line;
        }

        if($formatted){
            $formatted_results = [];

            $columns = [];
            foreach($query_results as $k=>$result){
                foreach($result as $col=>$r) {
                    if(preg_match('/([a-z]*)\-([a-z]*)/', $col, $cols)){
                        if(!in_array($cols[1], $columns)) $columns[] = $cols[1];
                        $formatted_results[$result['id']][$cols[1]] = [];
                    }else{
                        $formatted_results[$result['id']][$col] = $r;
                    }
                }
            }
            foreach($columns as $single_col){
                foreach($query_results as $k=>$result) {
                    foreach($result as $col=>$r) {
                        if (strstr($col, $single_col)) {
                            if($r) $formatted_results[$result['id']][$single_col][$result[$single_col . "-id"]][explode("-", $col)[1]] = $r;
                        }
                    }
                }
            }
            $query_results = $formatted_results;
        }

        return $query_results;
    }

//    echo "<pre>";
//    db_query('posts',[['id',$_GET['id']]],[['users','author_id','id']]);
//    echo "</pre>";
?>